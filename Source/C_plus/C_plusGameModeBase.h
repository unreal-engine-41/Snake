// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "C_plusGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class C_PLUS_API AC_plusGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
