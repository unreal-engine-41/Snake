// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef C_PLUS_C_plusGameModeBase_generated_h
#error "C_plusGameModeBase.generated.h already included, missing '#pragma once' in C_plusGameModeBase.h"
#endif
#define C_PLUS_C_plusGameModeBase_generated_h

#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_SPARSE_DATA
#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_RPC_WRAPPERS
#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAC_plusGameModeBase(); \
	friend struct Z_Construct_UClass_AC_plusGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AC_plusGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/C_plus"), NO_API) \
	DECLARE_SERIALIZER(AC_plusGameModeBase)


#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAC_plusGameModeBase(); \
	friend struct Z_Construct_UClass_AC_plusGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AC_plusGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/C_plus"), NO_API) \
	DECLARE_SERIALIZER(AC_plusGameModeBase)


#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AC_plusGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AC_plusGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AC_plusGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AC_plusGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AC_plusGameModeBase(AC_plusGameModeBase&&); \
	NO_API AC_plusGameModeBase(const AC_plusGameModeBase&); \
public:


#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AC_plusGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AC_plusGameModeBase(AC_plusGameModeBase&&); \
	NO_API AC_plusGameModeBase(const AC_plusGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AC_plusGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AC_plusGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AC_plusGameModeBase)


#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define C_plus_Source_C_plus_C_plusGameModeBase_h_12_PROLOG
#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_SPARSE_DATA \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_RPC_WRAPPERS \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_INCLASS \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C_plus_Source_C_plus_C_plusGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_SPARSE_DATA \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	C_plus_Source_C_plus_C_plusGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> C_PLUS_API UClass* StaticClass<class AC_plusGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C_plus_Source_C_plus_C_plusGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
